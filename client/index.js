import m from 'mithril';
import { Index } from 'views/Index';
import { gstate } from 'gstate';
import './styles/main.css';

gstate.init();
m.route.prefix('');

m.route(document.getElementById('app'), '/', {
    '/': {
        render() {
            return m(Index);
        }
    }
});