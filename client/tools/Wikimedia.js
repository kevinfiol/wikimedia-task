import m from 'mithril';

export const Wikimedia = {
    getPages(category) {
        return m.request({
            method: 'POST',
            url: '/client/getPages/',
            data: { category: category, limit: 50 }
        });
    }
};