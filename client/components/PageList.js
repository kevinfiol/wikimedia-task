import m from 'mithril';

const Page = {
    pageUrl: null,
    extract: null,
    
    oninit({state, attrs}) {
        const page = attrs.title.split(' ').join('_');
        state.pageUrl = `https://en.wikipedia.org/wiki/${page}`;
        state.extract = attrs.extract.length > 500
            ? `${attrs.extract.substring(0, 500)}...`
            : attrs.extract
        ;
    },

    view({state, attrs}) {
        return m('.col.col-12.p2.my1.rounded.result', [
            m('p.h3', m('a', { href: state.pageUrl }, `${attrs.title} (${attrs.pageid})`)),
            m('p.h4', 'Readability Score:', m('.bold.green.h3', attrs.readability)),
            m('p', state.extract)
        ]);
    }
};

export const PageList = {
    view({attrs}) {
        return [
            m('p.h3.p1.result.rounded', `${attrs.pages.length} result(s) for `, m('span.bold', attrs.term)),
            m('.my3.card', attrs.pages.map(page => m(Page, page)))
        ];
    }
};