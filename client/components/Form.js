import m from 'mithril';
import stream from 'mithril/stream';
import { gstate } from 'gstate';
import { Wikimedia } from 'tools/Wikimedia';

export const Form = {
    input: null,

    oninit({state}) {
        state.input = stream('');
    },

    view({state}) {
        return m('.clearfix.my2', [
            m('label', [
                'Enter a ', 
                m('a', { href: 'https://en.wikipedia.org/wiki/Portal:Contents/Categories' }, 'category'), 
                ' name.'
            ]),

            m('input.input.bg-black.white.my1', {
                type: 'text',
                placeholder: 'e.g. Physics',
                oninput: m.withAttr('value', state.input),
                onkeyup: (ev) => {
                    if (ev.keyCode === 13
                        && state.input().trim()
                        && !gstate.loading()
                    ) {
                        search(state.input());
                    }
                }
            }),

            m('button.btn.btn-outline.left', {
                disabled: !state.input().trim() || gstate.loading(),
                onclick: () => {
                    if (state.input() && !gstate.loading()) search(state.input());
                }
            }, 'search')
        ]);
    }
};

function search(input) {
    gstate.term(input.trim());
    gstate.pages(null);
    gstate.loading(true);
    gstate.message(null);

    Wikimedia.getPages(input.trim())
        .then((res) => {
            if (res.length < 1) {
                gstate.message('No Results Found');
            }

            gstate.pages(res);
            gstate.loading(false);
        })
        .catch(() => {
            gstate.message('An Error Occured :(');
            gstate.loading(false);
        })
    ;
}