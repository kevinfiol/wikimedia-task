import m from 'mithril';
import { gstate } from 'gstate';
import { Form } from 'components/Form';
import { PageList } from 'components/PageList';


export const Index = {
    view() {
        return m('.px3', [
            m('a.h1.white', { href: '/' }, 'Wikipedia Category Readability Search'),
            m(Form),

            m('p.h5', [
                '* Readability is scored with Flesch-Kincaid Reading Ease test using ',
                m('a', { href: 'https://github.com/DaveChild/Text-Statistics' }, 'DaveChild/Text-Statistics.')
            ]),

            m('p.h5', '* Results in ascending order.'),

            gstate.pages() && gstate.pages().length > 0
                ? m(PageList, { pages: gstate.pages(), term: gstate.term() })
                : null
            ,

            gstate.loading()
                ? m('.loading.p3')
                : null
            ,

            gstate.message()
                ? m('.h3.p4.my3.center.rounded.result', gstate.message())
                : null
            ,
        ]);
    }
};