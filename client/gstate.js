import stream from 'mithril/stream';

export const gstate = {
    term: null,
    pages: null,
    loading: null,
    message: null,

    init() {
        this.term = stream('');
        this.pages = stream(null);
        this.loading = stream(false);
        this.message = stream(null);
    }
};