Wikipedia Category Readability Search 
===

### [Demo](https://wiki-readability.herokuapp.com/)

This is a small app that lists results for a given Wikipedia category, sorted by its respective [Flesch-Kincaid Reading Ease Score](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests).

This app uses the following APIs:
* [API:Categorymembers](https://www.mediawiki.org/wiki/API:Categorymembers)
* [Extension:TextExtracts](https://www.mediawiki.org/wiki/Extension:TextExtracts#API)

## Build & Run
* Requires [Yarn](https://yarnpkg.com/en/) and [Composer](https://getcomposer.org/)

1. Get Composer dependencies

```
composer install
```

2. Get Yarn dependencies

```
yarn
```

3. Bundle client code

```
yarn run build
```

*Outputs to `web/app.js` and `web/css/main.css`*

4. Configure web server to reroute all requests to `web/index.php`. If using Apache, you may use the included `web/.htaccess`.

5. Navigate to localhost to run application.

## Project Directories

```
client/
    components/     -- View Components
    styles/         -- Pre-bundled CSS
    tools/          -- Utilities & Request Objects
    views/          -- Base Views
    gstate.js       -- Global State Object
    index.js        -- Client App Bootstrap & Routing
server/
    Controllers/    -- Route Controllers
    Services/       -- Service Objects
    Templates/      -- Template Files
    App.php         -- Application Bootstrap
    Dependencies    -- Dependency Specifications for DI
    Routes          -- Application Routes
web/
    css/
    index.php
```

## Wishlist
* Write Server-side and Client-side Tests
* Provide sorting options in UI (allow user to sort descending, by name, etc.)
* Catch more edge cases (request timeouts, etc.)
* Add Category autosuggest
* Cache results