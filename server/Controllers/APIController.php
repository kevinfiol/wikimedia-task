<?php declare(strict_types = 1);

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use App\Services\WikimediaService;

class APIController
{
    private $wikiService;

    public function __construct(WikimediaService $service)
    {
        $this->wikiService = $service;
    }

    private function sanitizeParams(array $params)
    {
        foreach ($params as $key => $val) {
            $params[$key] = filter_var($val, FILTER_SANITIZE_STRING);
        }

        return $params;
    }

    public function getPages(Request $req, Response $res)
    {
        $params = $this->sanitizeParams($req->getParsedBody());
        $titles = $this->wikiService->getCategoryTitles($params['category'], $params['limit']);
        $pages = $this->wikiService->getPages($titles);
        $pages = $this->wikiService->scorePageExtracts($pages);

        usort($pages, function($a, $b) {
            return $a['readability'] > $b['readability'];
        });

        return $res->withJson($pages);
    }
}