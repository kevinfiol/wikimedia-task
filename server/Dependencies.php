<?php declare(strict_types = 1);

use Slim\Views\PhpRenderer;
use GuzzleHttp\Client;
use DaveChild\TextStatistics\TextStatistics;

use App\Controllers\IndexController;
use App\Controllers\APIController;
use App\Services\WikimediaService;

return [
    'view' => function() {
        return new PhpRenderer(__DIR__ . '/Templates/');
    },

    'GuzzleHttp\Client' => function() {
        return new Client();
    },

    'DaveChild\TextStatistics\TextStatistics' => function() {
        return new TextStatistics();
    },

    'App\Services\WikimediaService' => function($c) {
        $client = $c->get('GuzzleHttp\Client');
        $textStat = $c->get('DaveChild\TextStatistics\TextStatistics');
        return new WikimediaService($client, $textStat);
    },

    'App\Controllers\IndexController' => function ($c) {
        $view = $c->get('view');
        return new IndexController($view);
    },

    'App\Controllers\APIController' => function($c) {
        $service = $c->get('App\Services\WikimediaService');
        return new APIController($service);
    }
];