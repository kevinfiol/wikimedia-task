<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
    <title>Wikipedia Category Readability Search</title>
</head>
    <body class="max-width-4 mx-auto my3 white bg-black">
        <noscript class="center p3 h2">
            You must have JavaScript enabled to view this page. :(
        </noscript>
        <div id="app"></div>
        <script src="/app.js"></script>
    </body>
</html>