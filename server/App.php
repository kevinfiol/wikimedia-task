<?php declare(strict_types = 1);

namespace App;

use Slim\App;
use Slim\Container;

require_once __DIR__ . '/../vendor/autoload.php';

$container = new Container();
$dependencies = include_once __DIR__ . '/Dependencies.php';

foreach ($dependencies as $name => $factory) {
    $container[$name] = $factory;
}

$app = new App($container);
$routes = include_once __DIR__ . '/Routes.php';

foreach ($routes as $path => list($http, $handler)) {
    $app->map($http, $path, $handler);
}

$app->run();