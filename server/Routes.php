<?php declare(strict_types = 1);

return [
    '/' => [['GET'], 'App\Controllers\IndexController:index'],
    '/client/getPages/' => [['POST'], 'App\Controllers\APIController:getPages']
];