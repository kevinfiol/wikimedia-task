<?php declare(strict_types = 1);

namespace App\Services;

use GuzzleHttp\Client;
use DaveChild\TextStatistics\TextStatistics;

class WikimediaService
{
    protected $client;
    private $textStat;
    private $baseUrl;

    public function __construct(Client $client, TextStatistics $textStat)
    {
        $this->client = $client;
        $this->textStat = $textStat;
        $this->baseUrl = 'https://en.wikipedia.org/w/api.php?';
    }

    public function request(array $params, string $method = 'GET')
    {
        $query = http_build_query($params);
        $url = "{$this->baseUrl}{$query}";

        $res = $this->client->request($method, $url);
        return json_decode($res->getBody()->getContents(), true);
    }

    public function getCategoryTitles($category, $limit)
    {
        $category = trim(strtolower($category));

        $res = $this->request([
            'action' => 'query',
            'list' => 'categorymembers',
            'format' => 'json',
            'cmtitle' => "Category:{$category}",
            'cmlimit' => $limit
        ]);

        if (isset($res['query']['categorymembers'])) {
            $titles = array_map(function($member) {
                return $member['title'];
            }, $res['query']['categorymembers']);

            return $titles;
        }

        return [];
    }

    public function getPages($titles)
    {
        $chunks = array_chunk($titles, 20);

        $res = array_map(function($chunk) {
            return $this->request([
                'action' => 'query',
                'prop' => 'extracts',
                'exintro' => 1,
                'explaintext' => '1',
                'format' => 'json',
                'titles' => implode('|', $chunk)
            ]);
        }, $chunks);

        $pages = array_reduce($res, function($acc, $extract) {
            return array_merge($acc, $extract['query']['pages']);
        }, []);

        return $pages;
    }

    public function scorePageExtracts($pages)
    {
        $pages = array_map(function($page) {
            $page['readability'] = $this->textStat->fleschKincaidReadingEase($page['extract']);
            return $page;
        }, $pages);

        return $pages;
    }
}